extern crate main_loop;
extern crate winit;

use main_loop::ControlFlow;
use std::sync::atomic::{AtomicBool, AtomicUsize, Ordering};
use std::sync::Arc;
use winit::{EventsLoop, Window};

fn main() {
    let mut events_loop = EventsLoop::new();
    let _window = Window::new(&events_loop).unwrap();

    let done = Arc::new(AtomicBool::new(false));
    let count = AtomicUsize::new(0);

    let d = done.clone();
    main_loop::winit::run(&mut events_loop, move |events, ms| {
        println!("{:?}", ms);

        for event in events {
            println!("{:?}", event);
        }

        if count.fetch_add(1, Ordering::SeqCst) >= 60 {
            d.store(true, Ordering::SeqCst);
            ControlFlow::Break
        } else {
            ControlFlow::Continue
        }
    });

    assert_eq!(done.load(Ordering::SeqCst), true);
}
