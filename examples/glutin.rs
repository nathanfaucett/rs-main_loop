extern crate gl;
extern crate glutin;
extern crate main_loop;

use glutin::GlContext;
use main_loop::ControlFlow;
use std::sync::atomic::{AtomicBool, AtomicUsize, Ordering};
use std::sync::Arc;

fn main() {
    let mut events_loop = glutin::EventsLoop::new();
    let window = glutin::WindowBuilder::new().with_title("Glutin");
    let context = glutin::ContextBuilder::new().with_vsync(true);
    let gl_window = glutin::GlWindow::new(window, context, &events_loop).unwrap();

    unsafe {
        gl_window.make_current().unwrap();
    }

    unsafe {
        gl::load_with(|symbol| gl_window.get_proc_address(symbol) as *const _);
        gl::ClearColor(0.0, 0.0, 0.0, 1.0);
    }

    let done = Arc::new(AtomicBool::new(false));
    let count = AtomicUsize::new(0);

    let d = done.clone();

    main_loop::glutin::run(&mut events_loop, move |events, ms| {
        println!("{:?}", ms);

        for event in events {
            println!("{:?}", event);
        }

        unsafe {
            gl::Clear(gl::COLOR_BUFFER_BIT);
        }

        gl_window.swap_buffers().unwrap();

        if count.fetch_add(1, Ordering::SeqCst) >= 60 {
            d.store(true, Ordering::SeqCst);
            ControlFlow::Break
        } else {
            ControlFlow::Continue
        }
    });

    assert_eq!(done.load(Ordering::SeqCst), true);
}
