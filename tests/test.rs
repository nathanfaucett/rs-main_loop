extern crate main_loop;

use std::sync::Arc;
use std::sync::atomic::{AtomicBool, AtomicUsize, Ordering};
use main_loop::ControlFlow;

#[test]
fn test() {
    let done = Arc::new(AtomicBool::new(false));
    let count = AtomicUsize::new(0);

    let d = done.clone();
    main_loop::run(move |_ms| {
        if count.fetch_add(1, Ordering::SeqCst) >= 60 {
            d.store(true, Ordering::SeqCst);
            ControlFlow::Break
        } else {
            ControlFlow::Continue
        }
    });

    assert_eq!(done.load(Ordering::SeqCst), true);
}
