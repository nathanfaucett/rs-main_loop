#[cfg(target_os = "emscripten")]
mod emscripten;

#[cfg(not(target_os = "emscripten"))]
mod native;

#[cfg(target_os = "emscripten")]
pub use self::emscripten::{run, set_target_fps, target_fps};

#[cfg(not(target_os = "emscripten"))]
pub use self::native::{run, set_target_fps, target_fps};
