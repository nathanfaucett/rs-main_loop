pub use ControlFlow::{Break, Continue};

#[derive(Clone, Copy, Debug, PartialEq, Eq, PartialOrd, Ord, Hash)]
pub enum ControlFlow {
    Continue,
    Break,
}
