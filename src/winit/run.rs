use lib_winit::{Event, EventsLoop};

use super::super::{run as platform_run, ControlFlow, Events};

#[inline]
pub fn run<F>(event_loop: &mut EventsLoop, mut callback: F)
where
    F: FnMut(&mut Events<Event>, f64) -> ControlFlow,
{
    let mut events = Events::new();

    platform_run(move |ms| {
        event_loop.poll_events(|event| events.push(event));
        callback(&mut events, ms)
    });
}
