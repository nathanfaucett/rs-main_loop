use time::precise_time_s;

#[inline]
pub fn now() -> f64 {
    precise_time_s() * 1000_f64
}
