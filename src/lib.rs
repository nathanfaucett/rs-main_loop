#[cfg(feature = "glutin")]
extern crate glutin as lib_glutin;

#[cfg(feature = "winit")]
extern crate winit as lib_winit;

extern crate time;

#[cfg(feature = "glutin")]
pub mod glutin;

#[cfg(feature = "winit")]
pub mod winit;

mod platform;
#[cfg(any(feature = "glutin", feature = "winit"))]
mod events;
mod control_flow;
mod now;

pub use self::control_flow::{Break, Continue, ControlFlow};
#[cfg(any(feature = "glutin", feature = "winit"))]
pub use self::events::Events;
pub use self::platform::{run, set_target_fps, target_fps};
pub use self::now::now;
