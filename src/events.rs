use std::collections::LinkedList;

pub struct Events<T>(LinkedList<T>);

impl<T> Events<T> {
    #[inline]
    pub fn new() -> Self {
        Events(LinkedList::new())
    }
    #[inline]
    pub fn push(&mut self, event: T) {
        self.0.push_front(event);
    }
}

impl<'a, T> Iterator for &'a mut Events<T> {
    type Item = T;

    #[inline]
    fn next(&mut self) -> Option<Self::Item> {
        self.0.pop_back()
    }
}
