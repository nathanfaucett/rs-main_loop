# main_loop

main loop for real time applications

## No Default features

```toml
main_loop = { version = "0.3", default-features = false }
```

```rust
extern crate main_loop;

use main_loop::ControlFlow;

fn main() {
    main_loop::run(|ms| {
        println!("{:?}ms since start", ms);
        ControlFlow::Continue
    });
}
```

## glutin

```toml
main_loop = { version = "0.3" }
```

```rust
extern crate glutin;
extern crate main_loop;

use main_loop::ControlFlow;

fn main() {
    let mut events_loop = glutin::EventsLoop::new();
    let window = glutin::WindowBuilder::new()
        .with_title("Glutin")
        .with_dimensions(512, 512);
    let context = glutin::ContextBuilder::new();
    let _gl_window = glutin::GlWindow::new(window, context, &events_loop).unwrap();


    main_loop::glutin::run(&mut events_loop, |events, ms| {
        println!("{:?}ms since start", ms);

        for event in events {
            println!("{:?}", event);
        }

        ControlFlow::Continue
    });
}
```

## winit

```toml
main_loop = { version = "0.3" }
```

```rust
extern crate main_loop;
extern crate winit;

use main_loop::ControlFlow;
use winit::{EventsLoop, Window};

fn main() {
    let mut events_loop = EventsLoop::new();
    let _window = Window::new(&events_loop).unwrap();

    main_loop::winit::run(&mut events_loop, |events, ms| {
        println!("{:?}ms since start", ms);

        for event in events {
            println!("{:?}", event);
        }

        ControlFlow::Continue
    });
}
```
